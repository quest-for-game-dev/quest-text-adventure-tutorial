---
id: clavis
title: Glossary
---

<dl>
  <dt><b>Interactive Fiction</b></dt>
  <dd>
  Software simulating environments in which players use text commands to control characters and influence the environment.
  <ul>
    <li><a target="_blank" href="https://en.wikipedia.org/wiki/Interactive_fiction">Interactive fiction on Wikipedia</a></li>
  </ul>
  </dd>

  <dt><b>Text Adventure</b></dt>
  <dd>
  A type of adventure game where the entire interface is text only.
  <ul>
    <li><a target="_blank" href="https://en.wikipedia.org/wiki/Text-based_game">Text-based game on Wikipedia</a></li>
    <li><a target="_blank" href="https://en.wikipedia.org/wiki/Adventure_game">Adventure game on Wikipedia</a></li>
  </ul>
  </dd>
</dl>

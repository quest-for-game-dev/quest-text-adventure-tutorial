---
id: what-is-a-text-adventure
title: What is a text adventure game?
---

In text based games, the player will take actions by entering text commands into a console window. These actions will be processed and applied to the world surrounding the hero. The outcome and effect of the player's action on the world will then be printed back to the console.

The game will continue with these 3 steps until the hero quits, die or walk away with pockets full of gold. Only one of these options will bring the hero ultimate fame and glory.

These repeating steps of input, update, render is the game loop. On each iteration of the game loop, the player and each monster or character in the game, will take one action. Then the game will wait for the player to make the next dicision, this is simular to turn based games where each player gets one turn per loop.

---
id: what-do-we-need-to-create-a-text-adventure
title: What do we need to create a text adventure game?
---

Creating an interactive text adventure game from scratch requires:

1. A programming language.
2. An integrated develoment environment (IDE) for the programming lanaguage.

### Learning a programming language

You only need to understand the fundamentals of programming, how to organize code, and some coding best practices to build your first text adventure game.

When learning a programming language the first steps, usually are to learn about these topics:

- Variables (string, number, boolean)
- Flow control (if, for, while)
- Functions or Methods
- Objects and Classes
- Reading text and numbers from the command line
- Printing text back to the command line

An introduction tutorial is usually enough to get you started. This makes a text adventure really accessible and easy to program for beginners.

---
id: introduction
title: Introduction
sidebar_label: Introduction
slug: /
---

The first quest in our game development journey is a text based adventure game. This entry step into game programming will require the would-be game dev Hero to choose a programming langauge which he or she can use to complete each quest.

Sticking to one language, framework or game engine, for now, will allow you to learn faster. Instead of jumping between different languages, tools and workflows, you can focus on understanding your chosen technology.

Text based games doesn’t require a rendering framework, any graphics or have to maintain a game loop of 60 FPS. We will explore graphics and sound in text based games. But for now let us start small...

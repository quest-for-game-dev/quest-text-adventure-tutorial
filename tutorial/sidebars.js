module.exports = {
  someSidebar: {
    Tutorial: [
      'introduction',
      'what-is-a-text-adventure',
      'why-create-a-text-adventure',
      'what-do-we-need-to-create-a-text-adventure',
      'how-do-we-create-text-adventure'
    ],
    Equipment: ['extra/clavis', 'extra/tools', 'extra/resources'],
  },
};

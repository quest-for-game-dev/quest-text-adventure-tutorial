module.exports = {
  title: 'Quest: Text Adventure',
  tagline: 'How to create a text adventure game',
  url: 'https://quest-for-game-dev.gitlab.io',
  baseUrl: '/quest-text-adventure-tutorial/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: 'quest-for-game-dev', // Usually your GitHub org/user name.
  projectName: 'quest-text-adventure-tutorial', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Quest: Text Adventure',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Tutorial',
          position: 'left',
        },
        { to: 'blog', label: 'Blog', position: 'left' },
        {
          href: 'https://gitlab.com/quest-for-game-dev/quest-text-adventure-tutorial',
          label: 'Source Code on GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Tutorial',
              to: 'docs/',
            },
            {
              label: 'Resources',
              to: 'docs/extra/resources/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitHub',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/quest-for-game-dev/quest-text-adventure-tutorial/-/tree/master/tutorial/',
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/quest-for-game-dev/quest-text-adventure-tutorial/-/tree/master/tutorial/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
